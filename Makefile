prepare:
	docker-compose run um rails db:setup
	docker-compose run stateman rails db:setup
	docker-compose run zaki rails db:setup
	docker-compose run formsub rails db:setup

hosts:
	docker network inspect zaki_cluster_default | bin/make_hosts

pull:
	git pull --recurse-submodules
	git submodule update --init --recursive
