# Technology

## Basics [pl]

1. Narzędzia to głównie framework ruby on rails. Aplikacji stoi na postgresie, na froncie klasycznie i prosto: html+css, javascript (jquery, coffee script). Architektura użytkowa to mikroserwisy: cztery aplikacje w wydzielonymi odpowiedzialnościami. Środowisko testowe jest na heroku, szczegóły na dole strony: [http://bit.ly/meet-zaki](http://bit.ly/meet-zaki)

2. Kod jest na gitlabie, do deployment używam narzędzia o nazwie capistrano. Docelowo aplikacje są wrzucone do vmki i ruch jest rozdzielany przez nginx reverse proxy.


# Cluster installation

## Latest version
1.  Install docker and docker compose on your system, install make, open your terminal (e.g. bash)
1. Clone the repo:
`git clone git@gitlab.com:modorg/zaki/zaki_cluster.git`
if you don't have ssh keys assosiated with gitlab, try this:
`git clone https://gitlab.com/modorg/zaki/zaki_cluster`

1. Open project directory: `cd zaki_cluster`

1. `make pull` # it should download content of sub repositories, you need to install make on windows to make it work
1. `make prepare` # it should create databases
1. `docker-compose up` # it should start applications at once, entrypoint for your browser is: http://zaki.dev.modorg.pl
1. If you encouter any problems in the beggining try to wait a bit longer or refresh the page - some caches should get fired up but it ought to work fine eventually
1. Initial login credentials: `admin@example.com`, password: `password` (type this word as password phrase)

## This might be old content
- Full installation: https://gitlab.com/modorg/techno/-/wikis/modorg-cluster-installation

# End user documentation

The below urls might be useful during Commettee configuration, although they are generally meant for real users in production environment.

## Candidate instruction
https://docs.google.com/document/d/1WzV650cfmW7LGxF1o_V1hnNAvTGuannH5z-S-x-h3Iw/edit

## Secretary instruction
https://docs.google.com/document/d/13J1z5WPtBBfkqY8a8QP4C_LpTutBs_cYhQ4rXoTrh30/edit#heading=h.xt71turyenof
